﻿
#ifndef BIRD_H
#define BIRD_H

#include "BaseFunction.h"
#include "BaseObject.h"
#include "ImpTimer.h"
#include "Map.h"

#define V0 -500
#define ACCELERATION 1625
#define BIRD_RADIUS 25.00
#define BIRD_SIZE 64.00
#define BIRD_HEIGHT 40.00

struct Circle // Tạo 1 hình tròn bao quanh con chim để kiểm tra va chạm với ống.
{
    int x, y;
    double r;
};

class Bird : public BaseObject
{
    public:
        Bird();

        ~Bird();

        enum birdType
        {
            baseType = 10,
            boughtType1 = 11,
            boughtType2 = 12,
        };

        void setTypeOfBird(const int& type) { typeOfBird = type;}

        int getTypeOfBird() const { return typeOfBird; }

        void setIsDead(const bool& dead) { isDead = dead; }

        bool getIsDead() const { return isDead; }

        bool getIsStarted() const { return started; }

        void handleAction(SDL_Event events);

        void handleMove();

        bool loadBirdImage(std :: string imgPath, SDL_Renderer* screen);

        void show(SDL_Renderer* des);

        void updateBirdImage(SDL_Renderer* des);

        Circle getColliderBird() const { return colliderBird; }

        void checkCollision(SDL_Rect cPipe, int cType);

        double distanceSquared(int x1, int y1, int x2, int y2);

        void setPoint(const int& p) { point += p; }

        int getPoint() const { return point; }

        void checkCollisionMap();

    private:
        int typeOfBird; // Nhân vật chim đang sử dụng là con nào.
        int xVal, yVal;
        int point;
        bool isDead;
        bool started;
        double basePos;
        Circle colliderBird;
};

#endif