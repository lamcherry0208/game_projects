﻿
#ifndef IMP_TIMER_H
#define IMP_TIMER_H   

class ImpTimer // Hàm xử lý việc pause, start, stop game.
{
    public:
        ImpTimer();

        ~ImpTimer();

        void start(); 

        void stop();

        void paused();

        void unPaused();

        int getTick();

        bool isStarted();
        
        bool isPaused();

    private:
        int startTick; // Biến kiểm soát việc đã ấn vào start game chưa.
        int pausedTick;
        bool is_Paused;
        bool is_Started;
};

#endif