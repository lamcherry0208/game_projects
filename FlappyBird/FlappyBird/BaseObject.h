﻿
#ifndef BASE_OBJECT_H
#define BASE_OBJECT_H
    
#include "BaseFunction.h"

class BaseObject
{
    public:
        BaseObject();

        ~BaseObject();

        void setRect(const double& x, const double& y) // Set góc trái trên cho ô hình ảnh.
        {
            gameRect.x = x;
            gameRect.y = y;
        }

        SDL_Rect getRect() { return gameRect; }

        SDL_Texture* getObject() { return gameObject; }

        virtual bool loadImage(std :: string imgPath, SDL_Renderer* screen);

        void render(SDL_Renderer* des); // render hình ảnh ra des.

        void freeImgData();

    protected:
        SDL_Texture* gameObject;
        SDL_Rect gameRect;
};

#endif