﻿
#ifndef PIPE_H
#define PIPE_H

#include "BaseFunction.h"
#include "BaseObject.h"
#include <vector>

#define PIPE_WIDTH  80
#define PIPE_TEXTURE_HEIGHT 500
#define PIPE_DISTANCE_VERTICAL 175 // Hằng số khoảng cách giữa hai cột trên dưới để con chim bay qua.
#define PIPE_DISTANCE_HORIZONTAL 300

class Pipe : public BaseObject
{
    public:
        Pipe();

        ~Pipe();

        bool loadPipeImage(std :: string imgPath, SDL_Renderer* screen);

        void movePipe(double xOffset, double bird_xPos);
            
        void setPipeType(const int& type) { pipeType = type; }

        void randomDoublePipe(int x); // Tạo ra các cặp cột có độ cao khác nhau

        void showPipe(SDL_Renderer* des);

        int get_yPosDownsidePipe() const { return yPosDownsidePipe; }

        bool get_isPointed() const { return isPointed; }

        void set_isPointed(const bool& p) { isPointed = p; }

    private:
        int xPos; // Hoành độ góc trái trên của cột trong quá trình di chuyển map liên tục. Hai cột trên dưới có tọa độ này giống nhau.
        int yPosDownsidePipe; // Tung độ góc trái trên của cột bên dưới. Cột bên trên không cần kiểm soát do tung độ chính bằng SCREEN_HEIGHT.
        bool isScored; // Đã vượt qua cột và ghi điểm.
        int maxY, minY; // Giới hạn tọa độ y của cột bên dưới.
        int pipeType;
        bool isPointed; // Cột đã được vượt qua hay chưa.
};      

#endif