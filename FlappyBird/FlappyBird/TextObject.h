
#ifndef TEXT_OBJECT_H_
#define TEXT_OBJECT_H_

#include "BaseFunction.h"

class TextObject
{
    public:

        TextObject();

        ~TextObject();

        enum TextColor
        {
            RED_TEXT = 0,
            WHITE_TEXT = 1,
            BLACK_TEXT = 2,
        };

        bool loadFromRenderText(TTF_Font* font, SDL_Renderer* screen);

        void free();

        void setColor(Uint8 red, Uint8 green, Uint8 blue);

        void setColorType(int type);

        void renderText(SDL_Renderer* screen, 
                        int xPos, int yPos, 
                        SDL_Rect* clip = NULL, 
                        double angle = 0.0, 
                        SDL_Point* center = NULL, 
                        SDL_RendererFlip flip = SDL_FLIP_NONE);
        
        int getTextWidth() const { return textWidth; }

        int getTextHeight() const { return textHeight; }

        void setTextContent(const std::string& text) { textContent = text; }

        std::string getTextContent() const { return textContent; }

    private:
        std::string textContent;
        SDL_Color textColor;
        SDL_Texture* textTexture;
        int textWidth, textHeight;

};

#endif