﻿
#include "stdafx.h"
#include "Bird.h"

ImpTimer birdTimer;

Bird :: Bird()
{
    typeOfBird = baseType;
    xVal = 0;
    yVal = 0;
    isDead = false;
    started = false;
    point = 0;
    gameRect.x = gameRect.y = gameRect.w = gameRect.h = 0;
}

Bird :: ~Bird()
{

}

bool Bird :: loadBirdImage(std :: string imgPath, SDL_Renderer* screen)
{
    bool ret = BaseObject :: loadImage(imgPath, screen);
    
    return ret;    
}

void Bird :: handleAction(SDL_Event events) // Bắt sự kiện bấm phím mũi tên lên
{
    if (events.type == SDL_KEYDOWN)
    {
        if (events.key.keysym.sym == SDLK_UP && events.key.repeat == 0)
        {
            if (started == false)
            {
                started = true;
            }

            basePos = gameRect.y; 
            birdTimer.start();
        }
    }
}

void Bird :: handleMove()
{
    if (started == true)
    {
         double fallingTime = birdTimer.getTick() / (double)1000;
         yVal = V0 * fallingTime + 0.5 * ACCELERATION * fallingTime * fallingTime;

         gameRect.y = basePos + yVal;
    }
}

double Bird :: distanceSquared(int x1, int y1, int x2, int y2)
{
    return sqrt((double)(x1 - x2) * (x1 - x2) + (double)(y1 - y2) * (y1 - y2));
}

void Bird :: checkCollisionMap()
{
    if (gameRect.y < 0 || gameRect.y + gameRect.h >= SCREEN_HEIGHT - groundVertical) // Nếu tọa độ y của con chim bị va chạm vào màn hình hoặc ground => thoát.
    {
        isDead = true;
    }
}

void Bird :: checkCollision(SDL_Rect cPipe, int cType)
{
    int xCenter = gameRect.x + BIRD_SIZE / 2;
    int yCenter = gameRect.y + BIRD_HEIGHT / 2;
    double radius = (double)sqrt((BIRD_HEIGHT / 2) * (BIRD_HEIGHT / 2) + (BIRD_SIZE / 2) * (BIRD_SIZE / 2));
    // colliderBird là hình tròn chứa tâm con chim. đâu hàm check va chạm đâu
    colliderBird.x = xCenter;
    colliderBird.y = yCenter;

    std :: pair < int, int > closetPoint; // Điểm gần tâm con chim nhất trên khung hình chứa cái cột.

    if (cType == 1) // va chạm cột trên.
    {
        if (colliderBird.x < cPipe.x)
        {
            closetPoint.first = cPipe.x;
            colliderBird.r = radius / 2;
        }
        else if (colliderBird.x > cPipe.x + cPipe.w)
        {
            closetPoint.first = cPipe.x + cPipe.w;
            colliderBird.r = radius / 2;
        }
        else 
        {
            closetPoint.first = colliderBird.x;
            colliderBird.r = (double)(BIRD_HEIGHT / 2);
        }

        if (colliderBird.y > cPipe.y + cPipe.h)
        {
            closetPoint.second = cPipe.y + cPipe.h;
            colliderBird.r = (double)(radius / 2);
        }
        else
        {
            closetPoint.second = colliderBird.y;
            colliderBird.r = (double)(BIRD_SIZE / 2);
        }
    }
    else if (cType == 2)// va chạm cột dưới.
    {
        if (colliderBird.x < cPipe.x)
        {
            closetPoint.first = cPipe.x;
            colliderBird.r = radius / 2;
        }
        else if (colliderBird.x > cPipe.x + cPipe.w)
        {
            closetPoint.first = cPipe.x + cPipe.w;
            colliderBird.r = radius / 2;
        }
        else 
        {
            closetPoint.first = colliderBird.x;
            colliderBird.r = (double)(BIRD_HEIGHT / 2);
        }

        if (colliderBird.y < cPipe.y)
        {
            closetPoint.second = cPipe.y;
            colliderBird.r = radius / 2;
        }
        else 
        {
            closetPoint.second = colliderBird.y;
            colliderBird.r = (double)(BIRD_SIZE / 2);
        }
    }

    if (distanceSquared(colliderBird.x, colliderBird.y, closetPoint.first, closetPoint.second) < colliderBird.r)
        isDead = true; // Có va chạm.
    else 
        isDead = false; // Không va chạm.
}

void Bird :: show(SDL_Renderer* des)
{
    
    if (gameRect.y >= 526)
    {
        int a = 5;
    }

    BaseObject :: render(des);
}



