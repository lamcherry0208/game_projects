﻿// FlappyBird.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "BaseFunction.h"
#include "BaseObject.h"
#include "Bird.h"
#include "ImpTimer.h"
#include "Map.h"
#include "Pipe.h"
#include <vector>
#include <cstdlib>
#include <ctime>
#include "TextObject.h"

BaseObject gameBackground;
TTF_Font* fontTime = NULL;
TTF_Font* fontMenu = NULL;

bool initData() // Khởi tạo các thông số cơ bản cho SDL.
{
	bool success = true;

	int ret = SDL_Init(SDL_INIT_VIDEO);
	if (ret < 0) // Hàm có lỗi => Init không thành công.
		return false;

	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"); // Thiết lập chất lượng hình ảnh.

    // Tạo một cửa sổ game với vị trí mặc định, kích thước và có thể nhìn thấy.
    gameWindow = SDL_CreateWindow("FlappyBird SDL 2.0 - Lam Cherry", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN); 
    if (gameWindow == NULL)
		success = false;
	else // Khởi tạo cửa sổ thành công thì bắt đầu khởi tạo màn hình bên trong khung cửa sổ.
	{
		gameScreen = SDL_CreateRenderer(gameWindow, -1, SDL_RENDERER_ACCELERATED);
		if (gameScreen == NULL)
			success = false;
		else // Khởi tạo thành công màn hình game.
		{
			SDL_SetRenderDrawColor(gameScreen, RENDER_DRAW_COLOR, RENDER_DRAW_COLOR, RENDER_DRAW_COLOR, RENDER_DRAW_COLOR); // Khởi tạo màu sắc cho màn hình.
			int imgFlags = IMG_INIT_PNG;
			if (!(IMG_Init(imgFlags) && imgFlags))
				success = false;
		}

        if (TTF_Init() == -1)
        {
            success = false;
        }
        
        fontTime = TTF_OpenFont("font//dlxfont.ttf", 15);
        if (fontTime == NULL)
        {
            success = false;
        }

        fontMenu = TTF_OpenFont("font//dlxfont.ttf", 60);
        if (fontMenu == NULL)
        {
            success = false;
        }
	}

    // Init audio
    if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
        success = false;
    else 
    {
        gameSound[0] = Mix_LoadWAV("sound//Melodia.wav");
        gameSound[1] = Mix_LoadWAV("sound//PassPipe.wav");
        gameSound[2] = Mix_LoadWAV("sound//GameOver.wav");
        gameSound[3] = Mix_LoadWAV("sound//JumpSound.wav");
        gameSound[4] = Mix_LoadWAV("sound//DeadSound.wav");

        if (gameSound[0] == NULL || gameSound[1] == NULL)
            success = false;
    }

	return success;
}

bool loadBackGround()
{
    bool ret = gameBackground.loadImage("Img//background.png", gameScreen);

    return ret;
}

void closeGame()
{
	gameBackground.freeImgData();

	SDL_DestroyRenderer(gameScreen);
	gameScreen = NULL;

    SDL_DestroyWindow(gameWindow);
	gameWindow = NULL;

	IMG_Quit();
	SDL_Quit();
}

std :: vector < std :: pair < Pipe, Pipe >  > makePipeList()
{
    std :: vector < std :: pair < Pipe, Pipe > > pipeList;
    pipeList.resize(5);
    int baseX = SCREEN_WIDTH + 1;

    srand(time(0));

    for (int i = 0; i < 5; ++i)
    {
        pipeList[i].first.loadPipeImage("Img//topPipe.png", gameScreen);
        pipeList[i].second.loadPipeImage("Img//bottomPipe.png", gameScreen);
        
        pipeList[i].first.setPipeType(1);
        pipeList[i].second.setPipeType(2);

        pipeList[i].first.randomDoublePipe(baseX);

        int yBottom = pipeList[i].first.get_yPosDownsidePipe();
        pipeList[i].second.setRect(baseX, yBottom);

        baseX += PIPE_DISTANCE_HORIZONTAL;

        if (baseX > 2 * SCREEN_WIDTH)
            baseX = SCREEN_WIDTH + PIPE_DISTANCE_HORIZONTAL;
    }
    
    return pipeList;
}

int main(int argc, char* argv[])
{
    bool isQuit = false;
    ImpTimer fpsTimer;

    if (!initData())
        return -1;

    //Make menu game 
    int retMenu = SDLBaseFunction :: showMenu(gameScreen, fontMenu, "PLAY GAME", "EXIT", "Img//MENU.png");
    if (retMenu == 1)
        isQuit = true;

    if (!loadBackGround())
    {
        return -1;
    }

    Map mapData;
    bool ret = mapData.loadGroundIMG("Img//ground.png", gameScreen);
    if (!ret)
    {
        return -1;
    }
    mapData.setRect(0, mapBorderY - groundVertical);

again_label:

    Mix_Pause(-1);
    Mix_PlayChannel(-1, gameSound[0], 0);
    Bird mainPlayer;
    ret = mainPlayer.loadBirdImage("Img//fl_bird.png", gameScreen);

    if (!ret)
    {
        return -1;
    }

    mainPlayer.setRect(SCREEN_WIDTH / 2 - BIRD_SIZE / 2, SCREEN_HEIGHT / 2 - BIRD_HEIGHT / 2);

    int mainPlayer_xPos = SCREEN_WIDTH / 2 - BIRD_HEIGHT / 2;

    TextObject gameTime; // Biến Text hiển thị thời gian đã chơi game.
    gameTime.setColorType(TextObject :: WHITE_TEXT);
    Uint32 delayTime = 0; // Thời gian delay lúc chưa nhấn bắt đầu game.

    TextObject gamePoint; // Biến Text hiển thị điểm người chơi.
    gamePoint.setColorType(TextObject :: RED_TEXT);

    std :: vector < std :: pair < Pipe, Pipe >  > showPipe = makePipeList();
    bool isWent[5] = {false, false, false, false, false};
    
    double backGround_xOffset = 0, pipe_xOffset = 2;

    srand(time(0));
    
    while (!isQuit)
    {
        while (SDL_PollEvent(&gameEvents) != 0)
        {
            if (gameEvents.type == SDL_QUIT)
                isQuit = true;

            mainPlayer.handleAction(gameEvents);
        }

        SDL_SetRenderDrawColor(gameScreen, RENDER_DRAW_COLOR, RENDER_DRAW_COLOR, RENDER_DRAW_COLOR, RENDER_DRAW_COLOR);
        SDL_RenderClear(gameScreen);
        
        // Set 2 background liên tục liền nhau và thay đổi tọa độ của background để đạt được hiệu ứng di chuyển màn hình liên tục.
        bool is_started = mainPlayer.getIsStarted();

        if (is_started == true) // Nếu người chơi đã bắt đầu game thì mới di chuyển màn hình theo.
        {
            backGround_xOffset -= (double)2;
            gameBackground.render(gameScreen);
            gameBackground.setRect(backGround_xOffset, 0);
            gameBackground.render(gameScreen);
            gameBackground.setRect(backGround_xOffset + SCREEN_WIDTH - 2, 0);

            if (backGround_xOffset <= -SCREEN_WIDTH)
            {
                backGround_xOffset = 0;
            }

            // Di chuyển các cột.
            for (int i = 0; i < 5; ++i)
            {
                showPipe[i].first.showPipe(gameScreen);
                showPipe[i].second.showPipe(gameScreen);

                showPipe[i].first.movePipe(pipe_xOffset, mainPlayer_xPos);
                showPipe[i].second.movePipe(pipe_xOffset, mainPlayer_xPos);
            
                // Check xem con chim đã đi qua cột i chưa, nếu đi qua rồi thì tăng thêm 1 điểm.
                if (showPipe[i].first.get_isPointed() == true && isWent[i] == false)
                {
                    mainPlayer.setPoint(1);
                    Mix_PlayChannel(-1, gameSound[1], 0);
                    isWent[i] = true;
                }
            
                SDL_Rect cur = showPipe[i].first.getRect();

                if (cur.x <= -PIPE_WIDTH)
                {
                    SDL_Rect lastPipe;
                
                    int lastID = (i == 0) ? 4 : i - 1;
                    lastPipe = showPipe[lastID].first.getRect();

                    showPipe[i].first.randomDoublePipe(lastPipe.x + PIPE_DISTANCE_HORIZONTAL);
                    showPipe[i].first.set_isPointed(false);
                    showPipe[i].second.set_isPointed(false);
                    isWent[i] = false;

                    int yBottom = showPipe[i].first.get_yPosDownsidePipe();
                    showPipe[i].second.setRect(lastPipe.x + PIPE_DISTANCE_HORIZONTAL, yBottom);
                }
            }
        }
        else // Ngược lại chỉ render màn hình gốc đứng yên.
        {
            gameBackground.render(gameScreen);
        }
        
        mainPlayer.handleMove();
        mainPlayer.show(gameScreen);

        if (is_started)
        {
            mapData.render(gameScreen);
            mapData.setRect(backGround_xOffset, SCREEN_HEIGHT - groundVertical);
            mapData.render(gameScreen);
            mapData.setRect(backGround_xOffset + SCREEN_WIDTH - 1, SCREEN_HEIGHT - groundVertical);
        }
        else 
        {
            mapData.render(gameScreen);
        }

        // Check va chạm bắt đầu từ đây. Check với map, xong check cột trên trước xong check cột dưới.
        mainPlayer.checkCollisionMap();

        if (!mainPlayer.getIsDead())
        {
            for (int i = 0; i < 5; ++i)
            {
                SDL_Rect cPipeUp = showPipe[i].first.getRect();
                SDL_Rect cPipeDown = showPipe[i].second.getRect();

                mainPlayer.checkCollision(cPipeUp, 1); // Check với cột trên.

                bool isDead = mainPlayer.getIsDead();
                if (isDead == true)
                {  
                    Mix_Pause(-1);
                    Mix_PlayChannel(-1, gameSound[4], 0);
                    break;
                }
                else // Nếu cột trên k va chạm thì check với cột dưới.
                {
                    mainPlayer.checkCollision(cPipeDown, 2);

                    isDead = mainPlayer.getIsDead();
                    if (isDead == true)
                    {     
                        Mix_Pause(-1);
                        Mix_PlayChannel(-1, gameSound[4], 0);
                        break;
                    }
                }
            }
        }
        else 
        {
            Mix_Pause(-1);
            Mix_PlayChannel(-1, gameSound[4], 0);
        }

        if (mainPlayer.getIsDead() == true)
        {
            Sleep(1000);

            Mix_PlayChannel(-1, gameSound[2], 0);

            int ret_menu = SDLBaseFunction::showMenu(gameScreen, fontMenu, "PLAY AGAIN", "EXIT", "img//MENU END.png");

            if (ret_menu == 1)
            {
                isQuit = true;
                continue;
            }
            else
            {
                isQuit = false;
                gameBackground.setRect(0, 0);
                mapData.setRect(0, SCREEN_HEIGHT - groundVertical);
                goto again_label;
            }
        }

        // Hiển thị thời gian của game và điểm giành được trên màn hình (text)
        std :: string strTime = "Played Time: ";
        Uint32 currentTime = SDL_GetTicks(); // Thời gian lúc vào game chưa ấn bắt đầu.
        if (is_started == false)
        {
            delayTime = SDL_GetTicks();
            currentTime = 0;
        }
        else 
        {
            currentTime = (SDL_GetTicks() - delayTime) / 1000;
        }
        std :: string strVal = std :: to_string(currentTime);
        strTime += strVal;
        gameTime.setTextContent(strTime);
        gameTime.loadFromRenderText(fontTime, gameScreen);
        gameTime.renderText(gameScreen, 25, 15);

        // Điểm game.
        std :: string strPoint = "Point: ";
        Uint32 currentPoint = (is_started == false) ? 0 : mainPlayer.getPoint();
        strPoint += std :: to_string(currentPoint);
        gamePoint.setTextContent(strPoint);
        gamePoint.loadFromRenderText(fontTime, gameScreen);
        gamePoint.renderText(gameScreen, SCREEN_WIDTH - 150, 15);

        SDL_RenderPresent(gameScreen);

        int realImpTime = fpsTimer.getTick(); // Thời gian thực sự đã trôi qua khi hiển thị 1 frame.
        int timePerFrame = 1000 / FRAME_PER_SECOND; // Mỗi frame sẽ chạy trong 1000 / 120 miliseconds.
        if (realImpTime < timePerFrame) // Nếu thời gian thực sự của 1 frame > thời gian quy định, ta cần tạo độ delay để 1 khung hình phải hiển thị đủ thời gian.
        {
            int delayTime = timePerFrame - realImpTime;
            if (delayTime >= 0)
                SDL_Delay(delayTime);
        }
    }

    closeGame();

	return 0;
}

