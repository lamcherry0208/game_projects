
#include "stdafx.h"
#include "Pipe.h"
#include <cstdlib>
#include <ctime>

Pipe :: Pipe()
{
    xPos = 0;
    yPosDownsidePipe = 0;
    isScored = 0;
    maxY = SCREEN_HEIGHT - GROUND_HEIGHT - 70;
    minY = maxY - PIPE_DISTANCE_VERTICAL - 100;
    pipeType = -1;
    isPointed = false;
}

Pipe :: ~Pipe()
{

}

bool Pipe ::loadPipeImage(std :: string imgPath, SDL_Renderer* screen)
{
    bool ret = BaseObject :: loadImage(imgPath, screen);

    return ret;
}

void Pipe :: randomDoublePipe(int x)
{
    if (pipeType == 1)
    {
        yPosDownsidePipe = rand() % (maxY - minY + 1) + minY;
        gameRect.x = x;
        gameRect.y = yPosDownsidePipe - PIPE_DISTANCE_VERTICAL - PIPE_TEXTURE_HEIGHT;
    }
}

void Pipe :: movePipe(double xOffset, double bird_xPos)
{
     gameRect.x -= xOffset;

     if (gameRect.x + gameRect.w < bird_xPos)
     {
         isPointed = true;
     }
}


void Pipe :: showPipe(SDL_Renderer* des)
{
    BaseObject :: render(des);
}