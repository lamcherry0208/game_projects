﻿/*
  File header khai báo trước các định danh, thư viện, các tên hàm, các tên biến cần sử dụng. Sau đó ta sẽ viết chi tiết code của chúng trong file cpp tương ứng.
  File này chứa tất cả các thông số cơ bản của project.
*/
#ifndef BASE_FUNCTION_H_
#define BASE_FUNCTION_H_

#include <windows.h>
#include <string>
#include <vector>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>
#include <sys/timeb.h>
#include <iostream>
#include "BaseObject.h"
#include "TextObject.h"

static SDL_Window *gameWindow = NULL; // Biến cửa sổ game.
static SDL_Renderer *gameScreen = NULL; // Biến màn hình game.
static SDL_Event gameEvents; // Biến bắt sự kiện (các thao tác từ chuột và bàn phím).

// Screen
const int SCREEN_WIDTH = 1280; // Chiều dài màn hình.
const int SCREEN_HEIGHT = 640; // Chiều cao màn hình.
const int SCREEN_BPP = 32; // Pixels.
const int FRAME_PER_SECOND = 120; // fps.

// Màu sắc trên màn hình
const int COLOR_KEY_R = 180;
const int COLOR_KEY_G = 180;
const int COLOR_KEY_B = 180;
const int RENDER_DRAW_COLOR = 0Xff;

// Game Object
const int BLOCK_WIDTH = 64;
const int BLOCK_NUM_ON_SCREEN = 5;
const int DIS_BETWEEN_BLOCK = (SCREEN_WIDTH - BLOCK_NUM_ON_SCREEN * BLOCK_WIDTH) / (BLOCK_NUM_ON_SCREEN + 1);
const int GROUND_HEIGHT = 100;

// Game Sound
static Mix_Chunk* gameSound[5]; // Có 4 sound: Background sound, sound pass ống, sound game over và sound khi con chim nhảy lên.

namespace SDLBaseFunction
{
    int showMenu(SDL_Renderer* g_screen, TTF_Font* font, 
                 const std::string& menu1, 
                 const std::string& menu2,
                 const std::string& img_name);
}

#endif

