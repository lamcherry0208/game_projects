
#include "stdafx.h"
#include "TextObject.h"

TextObject :: TextObject()
{
    textColor.r = textColor.g = textColor.b = 255;
    textTexture = NULL;
}

TextObject :: ~TextObject()
{

}

bool TextObject :: loadFromRenderText(TTF_Font* font, SDL_Renderer* screen)
{
    SDL_Surface* textSurface = TTF_RenderText_Solid(font, textContent.c_str(), textColor);

    if (textSurface != NULL)
    {
        textTexture = SDL_CreateTextureFromSurface(screen, textSurface);

        textWidth = textSurface -> w;
        textHeight = textSurface -> h;

        SDL_FreeSurface(textSurface);
    }

    return textSurface != NULL;
}

void TextObject :: free()
{
    if (textTexture != NULL)
    {
        SDL_DestroyTexture(textTexture);
        textTexture = NULL;
    }
}

void TextObject :: setColor(Uint8 red, Uint8 green, Uint8 blue)
{
    textColor.r = red;
    textColor.g = green;
    textColor.b = blue;
}

void TextObject :: setColorType(int type)
{
    if (type == RED_TEXT)
    {
        SDL_Color color = {255, 0, 0};
        textColor = color;
    }
    else if (type == WHITE_TEXT)
    {
        SDL_Color color = {255, 255, 255};
        textColor = color;
    }
    else if (type == BLACK_TEXT)
    {
        SDL_Color color = {0, 0, 0};
        textColor = color;
    }
}

void TextObject :: renderText(SDL_Renderer* screen, 
                              int xPos, int yPos, 
                              SDL_Rect* clip, 
                              double angle, 
                              SDL_Point* center, 
                              SDL_RendererFlip flip)
{
    SDL_Rect renderQuad = {xPos, yPos, textWidth, textHeight};

    if (clip != NULL)
    {
        renderQuad.w = clip -> w;
        renderQuad.h = clip -> h;
    }

    SDL_RenderCopyEx(screen, textTexture, clip, &renderQuad, angle, center, flip);
}