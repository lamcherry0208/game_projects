﻿
#include "stdafx.h"
#include "BaseObject.h"

BaseObject :: BaseObject()
{
    gameObject = NULL;
    gameRect.x = gameRect.y = gameRect.w = gameRect.h = 0;
}

BaseObject :: ~BaseObject()
{
    freeImgData();
}

bool BaseObject :: loadImage(std :: string imgPath, SDL_Renderer* screen) // Load ảnh lên sẵn để chờ Render, đồng thời kiểm tra xem có load thành công không.
{
    freeImgData();

    SDL_Texture* newTexture = NULL;
    SDL_Surface* loadSurface = IMG_Load(imgPath.c_str());

    if (loadSurface != NULL) 
    {
        UINT32 colorKey = SDL_MapRGB(loadSurface -> format, COLOR_KEY_R, COLOR_KEY_G, COLOR_KEY_B); // Colorkey để làm mất background của hình ảnh.
        SDL_SetColorKey(loadSurface, SDL_TRUE, colorKey);

        newTexture = SDL_CreateTextureFromSurface(screen, loadSurface);
        if (newTexture != NULL)
        {
            gameRect.w = loadSurface -> w;
            gameRect.h = loadSurface -> h;
        }

        SDL_FreeSurface(loadSurface);
    }

    gameObject = newTexture;

    return gameObject != NULL;
}

void BaseObject :: render(SDL_Renderer* des)
{
    SDL_Rect renderQuad = gameRect;

    SDL_RenderCopy(des, gameObject, NULL, &renderQuad); // Phủ toàn bộ hình ảnh đã load sẵn ở gameObject (thuộc tính NULL) lên des, renderQuad là vị trí và size ảnh.
}

void BaseObject :: freeImgData() // Xóa đối tượng hình ảnh đi để giải phóng bớt bộ nhớ.
{
    if (gameObject != NULL)
    {
        SDL_DestroyTexture(gameObject);
        gameObject = NULL;
        gameRect.w = gameRect.h = 0;
    }
}