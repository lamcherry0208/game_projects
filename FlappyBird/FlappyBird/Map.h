
#ifndef MAP_H
#define MAP_H
    
#include "BaseFunction.h"
#include "BaseObject.h"

#define mapBorderX 1280 
#define mapBorderY 640
#define groundVertical 100

class Map : public BaseObject
{
    public:
        Map();

        ~Map();

        bool loadGroundIMG(std :: string imgPath, SDL_Renderer* des);

        void show(SDL_Renderer* screen);

    private:
        int xPos, yPos;
};

#endif