﻿// Hàm định nghĩa những thành phần cơ bản trong 1 object ảnh

#ifndef BASE_OBJECT_H_
#define BASE_OBJECT_H_

#include "BaseFunction.h"

class BaseObject
{
	public:
		BaseObject(); // Constructor: Khởi tạo các thông số ban đầu.

		~BaseObject(); // Destructor: Xóa bỏ vùng nhớ của các biến sau khi chúng bị hủy đi => Tránh lãng phí vùng nhớ.

		void SetRect(const int& x, const int& y) // Hàm tạo kích thước cho khung hình ảnh.
		{
			rect_.x = x;
			rect_.y = y;
		}

		SDL_Rect GetRect() const // Hàm trả ra kích thước hình ảnh.
		{
			return rect_;
		}	

		SDL_Texture* GetObject() const // Hàm trả ra hình ảnh.
		{
			return p_object_; 
		}

		virtual bool loadImage(std::string path, SDL_Renderer* screen); // Load hình ảnh và đưa vào object để sử dụng. Dùng hàm ảo để lớp con có thể tái sử dụng và đa hình.

		void Render(SDL_Renderer* des, const SDL_Rect* clip = NULL);

		void Free(); // Giải phóng các biến đã sử dụng xong, tránh tồn đọng dữ liệu gây lãng phí bộ nhớ.

	protected:
		SDL_Texture* p_object_; // Biến lưu trữ hình ảnh.
		SDL_Rect rect_; // Biến lưu trữ kích thước hình ảnh.
};


#endif
