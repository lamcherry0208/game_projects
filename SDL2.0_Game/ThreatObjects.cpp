﻿#include "stdafx.h"
#include "ThreatObjects.h"

ThreatObjects :: ThreatObjects()
{
    frameWidth = frameHeight = 0;
    xVal = yVal = 0.0;
    xPos = yPos = 0.0;
    onGround = false;
    comeBackTime = 0;
    frameIndex = 0;
}

ThreatObjects :: ~ThreatObjects()
{

}

bool ThreatObjects :: loadImage(std :: string path, SDL_Renderer* screen)
{
    bool ret = BaseObject :: loadImage(path, screen);

    if (ret == true) // Load thành công
    {
        frameWidth = rect_.w / THREAT_FRAME_NUM;
        frameHeight = rect_.h;
    }

    return ret;
}

void ThreatObjects :: setClip()
{
    if (frameWidth > 0 && frameHeight > 0)
    {
        // Coi vị trí của 1 frame là tọa độ đỉnh trái trên của frame, cạnh trên của frame là trục Ox.
        // frameClip[0].x = frameClip[0].y = 0;
        // frameClip[0].w = frameWidth;
        // frameClip[0].h = frameHeight;
        for (int i = 0; i < 8; ++i)
        {
            frameClip[i].x = i * frameWidth; // Vị trí bắt đầu trên trục hoành của frame thứ i bằng i * độ rộng 1 frame
            frameClip[i].y = 0;
            frameClip[i].w = frameWidth;
            frameClip[i].h = frameHeight;
        }
    }
}

void ThreatObjects :: show(SDL_Renderer* des)
{
    if (comeBackTime == 0) // Cho phép hiển thị hình ảnh khi quái vật chưa chết.
    {
        rect_.x = xPos - mapX; // Lấy vị trí thực sự.
        rect_.y = yPos - mapY;
        ++frameIndex; // Đối với đối tượng quái vật thì nên thay đổi frame liên tục để quái vật luôn luôn di chuyển nhấp nháy.

        if (frameIndex >= 8) // Hết dải frame thì quay lại từ đầu dải.
            frameIndex = 0;

        SDL_Rect* currentClip = &frameClip[frameIndex];
        SDL_Rect renderQuad = {rect_.x, rect_.y, frameWidth, frameHeight};

        SDL_RenderCopy(des, p_object_, currentClip, &renderQuad);
    }
}

void ThreatObjects :: doThreat(Map& g_map)
{
    if (comeBackTime == 0) // quái vật rơi từ trên xuống.
    {
        xVal = 0;
        yVal += THREAT_GRAVITY_SPEED;

        if (yVal > THREAT_MAX_FALL_SPEED) // Rơi nhanh dần đều, nếu rơi đến tốc độ nhất định thì rơi đều.
            yVal = THREAT_MAX_FALL_SPEED;

        checkToMap(g_map);
    }
    else if (comeBackTime > 0)
    {
        --comeBackTime;

        if (comeBackTime == 0)
        {
            xVal = yVal = 0;

            if (xPos > 256)
                xPos -= 256;
            else 
                xPos = 0;

            yPos = 0;
            comeBackTime = 0;
        }
    }
}

void ThreatObjects :: checkToMap(Map& mapData)
{
    int x1 = 0, x2 = 0; // Giới hạn 1 đoạn kiểm tra trên trục hoành.
    int y1 = 0, y2 = 0; // Giới hạn 1 đoạn kiểm tra trên trục tung.

    // Check chiều ngang trước
    int heightMin = frameHeight < TILE_SIZE ? frameHeight : TILE_SIZE;
    // Xác định xem nhân vật đang nằm từ ô thứ bao nhiêu tới ô thứ bao nhiêu trên bản đồ.
    x1 = (xPos + xVal) / TILE_SIZE; // xác định nhân vật đang đứng ở ô thứ bao nhiêu, lấy vị trí hoành độ chia cho kích thước tile.
    x2 = (xPos + xVal + frameWidth - 1) / TILE_SIZE; // Điểm kết thúc của nhân vật, trừ 1 để đường biên của nhân vật k bị trùng đường biên của tile.
    y1 = (yPos + yVal) / TILE_SIZE;
    y2 = (yPos + heightMin - 1) / TILE_SIZE;

    if (x1 >= 0 && x2 < MAX_MAP_X && y1 >= 0 && y2 < MAX_MAP_Y) // Nếu nhân vật đang nằm trong bản đồ.
    {
        if (xVal > 0) // Nhân vật đang di chuyển sang phải.
        {
            int val1 = mapData.tile[y1][x2];
            int val2 = mapData.tile[y2][x2];

            if ((val1 != 0 && val1 != STATE_MONEY) || (val2 != 0 && val2 != STATE_MONEY)) // Quái vật đang chạm vào 1 ô có chướng ngại vật nhưng không phải tiền.
            {
                xPos = x2 * TILE_SIZE; 
                xPos -= frameWidth + 1; // Trả  vị trí của nhân vật về vị trí ngay trước vị trí bị va chạm.
                xVal = 0; // Gán lượng di chuyển về 0, kể cả có ấn di chuyển tiếp cũng vẫn sẽ bị va chạm.

                onGround = true;
            }
        }
        else if (xVal < 0) // Đang di chuyển sang trái.
        {
            int val1 = mapData.tile[y1][x1];
            int val2 = mapData.tile[y2][x1];

            if ((val1 != 0 && val1 != STATE_MONEY) || (val2 != 0 && val2 != STATE_MONEY)) // Vẫn là ô vật cản nhưng không phải tiền.
            {
                xPos = (x1 + 1) * TILE_SIZE;
                xVal = 0;
            }
        }
    }

    // Check chiều dọc xem có va chạm không.
    int widthMin = frameWidth < TILE_SIZE ? frameWidth : TILE_SIZE;
    x1 = xPos / TILE_SIZE;
    x2 = (xPos + widthMin) / TILE_SIZE;
    y1 = (yPos + yVal) / TILE_SIZE;
    y2 = (yPos + yVal + frameHeight - 1) / TILE_SIZE;

    if (x1 >= 0 && x2 < MAX_MAP_X && y1 >= 0 && y2 < MAX_MAP_Y)
    {
        if (yVal > 0)
        {
            int val1 = mapData.tile[y2][x1];
            int val2 = mapData.tile[y2][x2];

            if ((val1 != 0 && val1 != STATE_MONEY) || (val2 != 0 && val2 != STATE_MONEY))
            {
                yPos = y2 * TILE_SIZE;
                yPos -= (frameHeight + 1);
                yVal = 0;

                onGround = true;
            }
        }
        else if (yVal < 0)
        {
            int val1 = mapData.tile[y1][x1];
            int val2 = mapData.tile[y1][x2];

            if ((val1 != 0 && val1 != STATE_MONEY) || (val2 != 0 && val2 != STATE_MONEY))
            {
                yPos = (y1 + 1) * TILE_SIZE;
                yVal = 0;
            }
        }
    }

    xPos += xVal;
    yPos += yVal;

    if (xPos < 0)
    {
        xPos = 0;
    }
    else if (xPos + frameWidth > mapData.maxX)
    {
        xPos = mapData.maxX - frameWidth - 1;
    }

    if (yPos > mapData.maxY) // Khi nhân vật rơi xuống => vị trí chiều dọc của nhân vật bị vượt quá số ô chiều dọc của bản đồ.
        comeBackTime = 30; // Set thời gian cho nhân vật quay trở lại bản đồ delay đi một chút = 30 milisec
}