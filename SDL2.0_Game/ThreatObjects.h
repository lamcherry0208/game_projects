﻿
// Tạo đối tượng các quái vật trên bản đồ, giống hệt như đối tượng người chơi chính.
#ifndef THREAT_OBJECTS_H
#define THREAT_OBJECTS_H

#include "BaseFunction.h"
#include "BaseObject.h"
#define THREAT_FRAME_NUM 8
#define THREAT_GRAVITY_SPEED 0.8
#define THREAT_MAX_FALL_SPEED 10

class ThreatObjects : public BaseObject
{
    public:
        ThreatObjects();

        ~ThreatObjects();

        void set_xVal(const float& x_val) { xVal = x_val; }

        void set_yVal(const float& y_val) { yVal = y_val; }
        
        void set_xPos(const float& x_pos) { xPos = x_pos; }

        void set_yPos(const float& y_pos) { yPos = y_pos; }

        float get_xPos() const { return xPos; }

        float get_yPos() const { return yPos; }

        void setMapXY(const int& map_x, const int& map_y) { mapX = map_x; mapY = map_y; }

        void setClip();

        bool loadImage(std :: string path, SDL_Renderer* screen);

        void show(SDL_Renderer* des);

        int getFrameWidth() const { return frameWidth; }

        int getFrameHeight() const { return frameHeight; }

        void doThreat(Map& g_map);

        void checkToMap(Map& mapData);

    private:
        int mapX, mapY;
        float xPos, yPos;
        float xVal, yVal;
        bool onGround;
        int frameWidth, frameHeight;
        int frameIndex;
        int comeBackTime;
        SDL_Rect frameClip[THREAT_FRAME_NUM];
};

#endif 
