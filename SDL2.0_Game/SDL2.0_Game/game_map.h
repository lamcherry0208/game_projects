﻿
#ifndef GAME_MAP_H
#define GAME_MAP_H

#include "BaseObject.h"
#include "BaseFunction.h"
#define MAX_TYPE_TILES 20

class TileMat : public BaseObject // Đối tượng hình ảnh sẽ hiển thị trên từng tile
{
	public:
		TileMat() {;}

		~TileMat() {;}
};

class GameMap // Quản lý việc xây dựng bản đồ, điền các hình ảnh vào từng tile.
{
	public:
		GameMap() {;}

		~GameMap() {;}

		void LoadMap(char* fileName); // Load dữ liệu bản đồ từ file text. 
	
		void loadTiles(SDL_Renderer* screen); // Load hình ảnh tương ứng với kiểu tile: Hình ảnh 0 ứng với 0.png, 1 ứng với 1.png,...

		void drawMap(SDL_Renderer* screen); // Hàm xây dựng map, điền từng hình ảnh vào từng vị trí tương ứng. 

        Map getMap() const { return game_map_; } // Trả ra game_map_ do biến game_map_ là biến private, phải trả giá trị ra mới sử dụng được.

        void setMap(Map& mapData) { game_map_ = mapData; };

	private:
		Map game_map_; // Lưu trữ bản đồ vào struct game_map
		TileMat tile_mat[MAX_TYPE_TILES]; // Danh sách các kiểu bản đồ ứng với mỗi ô hình ảnh, đặt số lượng tối đa là 20 kiểu.
};
#endif
