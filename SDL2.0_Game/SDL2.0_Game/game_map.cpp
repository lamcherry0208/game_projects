﻿
#include "stdafx.h"
#include "game_map.h"

void GameMap::LoadMap(char* name)
{
	FILE* fp = NULL; 
	fopen_s(&fp, name, "rb"); // Đọc file từ tên file chuyển vào con trỏ fp
	if (fp == NULL) // Đọc file không thành công, return.
		return;

	game_map_.maxX = game_map_.maxY = 0; // Ban đầu bắt đầu fill từ ô (0, 0).
	for (int i = 0; i < MAX_MAP_Y; ++i) // Duyệt chỉ số tung độ (trên trục Oy nên đặt là MAX_MAP_Y)
	{
		for (int j = 0; j < MAX_MAP_X; ++j) // Duyệt chỉ số hoành độ trên trục Ox
		{
			fscanf_s(fp, "%d", &game_map_.tile[i][j]); // Đọc các giá trị số nguyên từ file fp vào ma trận game_map.
			int value = game_map_.tile[i][j];

			if (value > 0) // Ô này có kiểu hình bản đồ
			{
				if (j > game_map_.maxX)
					game_map_.maxX = j;

				if (i > game_map_.maxY)
					game_map_.maxY = i;
			}
		}
	}
	// Đưa maxX và maxY tới giá trị tối đa của bản đồ.
	game_map_.maxX = (game_map_.maxX + 1) * TILE_SIZE;
	game_map_.maxY = (game_map_.maxY + 1) * TILE_SIZE; 

	game_map_.startX = game_map_.startY = 0;
	game_map_.file_name = name;

	fclose(fp); // Đóng file lại sau khi đã load xong.
}

void GameMap::loadTiles(SDL_Renderer* screen)
{
	char file_img[20]; // Chỉ số các tấm ảnh.
	FILE* fp = NULL;

	for (int i = 0; i < MAX_TYPE_TILES; ++i)
	{
		sprintf_s(file_img, "map/%d.png", i); // Chuyển file ảnh sang dạng tên map0.png, map1.png,...

		fopen_s(&fp, file_img, "rb");
		if (fp == NULL) // Không đọc được tấm ảnh thì bỏ qua, chuyển sang tấm tiếp theo.
			continue;
		
		fclose(fp);

		tile_mat[i].LoadImage(file_img, screen); // Nếu load ảnh thành công thì tải sẵn ảnh lên để chờ render ra màn hình.
	}	
}

void GameMap::drawMap(SDL_Renderer* screen)
{
	int x1 = 0, x2 = 0;
	int y1 = 0, y2 = 0; // Chỉ số x, y đánh dấu là điền ảnh từ ô nào tới ô nào.
	int mapX = 0, mapY = 0; // Chỉ số ô thứ bao nhiêu trên bản đồ.

	mapX = game_map_.startX / TILE_SIZE; // Tọa độ trục ngang của ô mà điểm fill đang thuộc vào.
	x1 = (game_map_.startX % TILE_SIZE) * -1; // Bắt đầu fill map từ vị trí startX % TILE_SIZE, do màn hình dịch chuyển nên phải nhân với -1 để lùi startX về sau cho đủ.
	x2 = x1 + SCREEN_WIDTH + (x1 == 0 ? 0 : TILE_SIZE); // Điểm kết thúc fill.

	mapY = game_map_.startY / TILE_SIZE; // Tọa độ trục dọc.
	y1 = (game_map_.startY % TILE_SIZE) * -1;
	y2 = y1 + SCREEN_HEIGHT + (y1 == 0 ? 0 : TILE_SIZE);

	for (int i = y1; i < y2; i += TILE_SIZE)
	{
		mapX = game_map_.startX / TILE_SIZE;
		for (int j = x1; j < x2; j += TILE_SIZE)
		{
			int val = game_map_.tile[mapY][mapX]; // Vị trí ô thứ bao nhiêu trên màn hình.
			
			if (val > 0)
			{
				tile_mat[val].SetRect(j, i); // Thiết lập vị trí fill hình ảnh vào map.
				tile_mat[val].Render(screen); // Render hình ảnh ra màn hình.
			}

            mapX++;
		}

        mapY++;
	}
}