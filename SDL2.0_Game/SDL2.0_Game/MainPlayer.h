﻿
#ifndef MAIN_PLAYER_H
#define MAIN_PLAYER_H

#include < vector >
#include "BaseFunction.h"
#include "BaseObject.h"
#include "BulletObject.h"

#define GRAVITY_SPEED 0.8 // Tốc độ rơi của nhân vật trên bản đồ.
#define MAX_FALL_SPEED 10 // Tốc độ rơi tối đa của nhân vật, do càng rơi sẽ càng nhanh
#define PLAYER_SPEED 8 // Tốc độ di chuyển của nhân vật.
#define PLAYER_JUMP_VAL 16 // Vị trí của nhân vật khi nhảy lên cao.

class MainPlayer : public BaseObject
{
    public:
        MainPlayer();

        ~MainPlayer();

        enum walkType // Kiểu dữ liệu lưu trạng thái di chuyển sang trái hay phải.
        {
            WALK_NONE = 0,
            WALK_RIGHT = 1,
            WALK_LEFT = 2,
        };

        bool LoadImage(std::string path, SDL_Renderer* screen); // Khai báo lại hàm loadImage vì ảnh nhân vật có thông số kích thước riêng (đa hình).

        void show(SDL_Renderer* des); // Hiển thị nhân vật lên màn hình.

        void handleInputAction(SDL_Event events, SDL_Renderer* screen); // Bắt sự kiện từ bàn phím: Khi bấm nút di chuyển thì nhân vật hiện trên màn hình tương ứng.

        void setClip(); // Hàm xử lý hiệu ứng animation.

        void doPlayer(Map& mapData); // Xử lý việc di chuyển nhân vật trên bản đồ.

        void checkToMap(Map& mapData); // Xử lý va chạm của nhân vật với các vật thể trên bản đồ.

        void setMapXY(const int mapX_, const int mapY_) { mapX = mapX_; mapY = mapY_; }; // Set vị trí cho bản đồ khi nhân vật di chuyển.

        void centerEntityOnMap(Map& mapData); // Thông số bản đồ khi nhân vật di chuyển (thông số điểm biên bắt đầu của bản đồ).

        void updatePlayerIMG(SDL_Renderer* des);

        void setBulletList(std::vector < BulletObject* > bullet_list) // Setter
        {
            p_bullet_list = bullet_list;
        }

        std::vector < BulletObject* > getBulletList() const { return p_bullet_list; } // Getter

        void handleBullet(SDL_Renderer* des); // Hàm xử lý việc bắn viên đạn ra.

        void increaseMoney(); // Tăng tiền lên nếu ăn được.

    private:
        std::vector < BulletObject* > p_bullet_list; // Băng đạn (danh sách các viên đạn) của nhân vật.
        float x_val, y_val; // Hai biến kiểm soát độ dài quãng đường di chuyển khi bấm di chuyển sang trái sang phải thì đi bao nhiêu.
        float x_pos, y_pos; // Vị trí hiện tại của nhân vật.
        int frameWidth, frameHeight; // Kích thước thực sự của nhân vật ứng với 1 frame, do hình ảnh nhân vật là một dải ảnh gồm 8 frames.
        SDL_Rect frameClip[8]; // Lưu thông số từng frame trong dải frames: Vị trí bắt đầu - kết thúc, độ rộng và chiều cao.
        Moving moveType; // Biến lưu cách di chuyển: Up, Down, Left, Right.
        int frameIndex; // Lưu chỉ số của frame khi nhân vật di chuyển để đổi frame phù hợp.
        int movingStatus; // Biến trạng thái di chuyển: Nhân vật đang sang phải hay sang trái.
        bool onGround; // Nhân vật có đang nằm trên mặt đất hay không.
        int mapX, mapY; // Khi nhân vật di chuyển thì biên của bản đồ sẽ ở vị trí nào.
        int comeBackTime; // Khi nhân vật rơi xuống vực, cần chờ 1 khoảng thời gian nhỏ để nvật hiện lại trên màn hình (sẽ mất 1 mạng).
        int moneyReceived; // Số tiền nhân vật đã ăn được.
};

#endif