﻿
#include "stdafx.h"
#include "BulletObject.h"

BulletObject :: BulletObject()
{
    x_val = y_val = 0;
    isMoved = false;
    bulletType = BASE_BULLET;
}

BulletObject :: ~BulletObject()
{

}

void BulletObject :: loadBulletIMG(SDL_Renderer* des)
{
    if (bulletType == BASE_BULLET)
    {
        LoadImage("img//base_bullet.png", des);
    }
    else if (bulletType == LASER_BULLET) 
    {
        LoadImage("img//laser_bullet.png", des);
    }
}

void BulletObject :: loadBulletDirectIMG(SDL_Renderer* des)
{

}

void BulletObject :: handleMove(const int& xBorder, const int& yBorder) // Tạo hướng bắn của viên đạn.
{
    if (bulletDirect == DIR_RIGHT) // Viên đạn đang hướng sang phải.
    {
        rect_.x += x_val; // Vị trí của đối tượng hình ảnh trên màn hình.
        if (rect_.x > xBorder)  // Nếu vị trí vượt ngoài biên ngang => Viên đạn biến mất.
        {
            isMoved = false;
        }
    }
    else if (bulletDirect == DIR_LEFT)
    {
        rect_.x -= x_val;
        if (rect_.x < 0)
        {
            isMoved = false;
        }
    }
    else if (bulletDirect == DIR_UP)
    {
        rect_.y -= y_val; // Giảm chỉ số hàng dọc đi.
        if (rect_.y < 0)
        {
            isMoved = false;
        }
    }
    else if (bulletDirect == DIR_UP_LEFT)
    {
        rect_.x -= x_val;
        if (rect_.x < 0)
            isMoved = false;
        
        rect_.y -= y_val;
        if (rect_.y < 0)
            isMoved = false;
    }
    else if (bulletDirect == DIR_UP_RIGHT)
    {
        rect_.x += x_val;
        if (rect_.x > xBorder)
            isMoved = false;
        
        rect_.y -= y_val;
        if (rect_.y < 0)
            isMoved = false;
    }
    else if (bulletDirect == DIR_DOWN_LEFT)
    {
        rect_.x -= x_val;
        if (rect_.x < 0)
            isMoved = false;
        
        rect_.y += y_val;
        if (rect_.y > yBorder)
            isMoved = false;
    }
    else if (bulletDirect == DIR_DOWN_RIGHT)
    {
        rect_.x += x_val;
        if (rect_.x > xBorder)
            isMoved = false;
        
        rect_.y += y_val;
        if (rect_.y > yBorder)
            isMoved = false;
    }
}