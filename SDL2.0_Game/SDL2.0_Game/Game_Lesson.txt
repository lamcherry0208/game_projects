﻿Bài 1: Tạo và liên kết thư viện SDL2.0.

Bài 2: Xây dựng khung chương trình game C++: Áp dụng nguyên lý lập trình module hóa (oop):
       Chia chương trình thành nhiều phần:
		+ Các file Header: Khai báo trước các giá trị const, các tên hàm, các thư viện cần sử dụng.
		+ Các file .cpp tương ứng với Header: Định nghĩa cụ thể các hàm.
		+ File main: Code chính.
	   Tạo các class đối tượng khác nhau, lưu trữ các thông tin cơ bản của đối tượng.

Bài 3: Kĩ thuật Tile Map: Xây dựng bản đồ có tương tác với nhân vật:
	   + Chia bản đồ thành các ô vuông khác nhau (tile) trên màn hình, kích thước bằng nhau; tùy theo kích thước màn hình để chọn kích thước ô vuông.
	   + Điền các đối tượng hình ảnh vào từng ô vuông.
	   + Trạng thái của bản đồ (trạng thái các ô có hình ảnh như thế nào) được lưu trữ trong 1 file dữ liệu (đánh bằng số để thể hiện ô đó có hình ảnh loại gì, các hình ảnh
	     cùng loại thì đánh cùng số).
	   + Cần xây dựng một cấu trúc dữ liệu để lưu trữ 3 thông số của một tile: Trạng thái ô, vị trí ô (x và y), ô thứ bao nhiêu.
	   + Tiếp theo cần xây dựng đối tượng hình ảnh để điền vào các tile. Thực tế có thể sử dụng BaseObject để đại diện cho đối tượng hình ảnh này, tuy nhiên BaseObject chỉ
	     là những đặc tính chung nhất của các đối tượng hình ảnh, nên chúng ta sẽ xây dựng đối tượng kế thừa từ BaseObject để thể hiện hình ảnh trên map.

Bài 4: Load nhân vật và hiệu ứng động animation + Xử lý va chạm và di chuyển của nhân vật.
	- Xây dựng một lớp MainPlayer để quản lý nhân vật chính. Lớp này được kế thừa từ lớp đối tượng hình ảnh BaseObject. Tuy nhiên do hình ảnh nhân vật có những thông
	  số riêng biệt, do đó ta sẽ sử dụng đa hình để hiệu chỉnh và thêm một vài tính năng.
	- Kích thước 1 frame nhân vật buộc phải nhỏ hơn hoặc bằng 1 tile, mục đích để xử lý những tình huống nhân vật bị rơi xuống hoặc bị chết.
	- Xây dựng hàm doPlayer để kiểm soát di chuyển của nhân vật, trong hàm này ta sẽ kiểm tra việc nhân vật di chuyển như thế nào (trái phải trên dưới), trong quá 
	  trình di chuyển va chạm phải map ra sao (sử dụng hàm checkToMap và hàm centerEntityOnMap gọi trong hàm doPlayer).

Bài 5: Xử lý thời gian và fps trong game: 
	+ Xử lý thời gian trong game: Bao nhiêu lâu đã trôi qua từ lúc bắt đầu hiển thị hình ảnh, lúc paused game và lúc start game.
	+ FPS: Chất lượng đồ họa (số frames hiển thị / 1 giây). Tạo chất lượng fps = 25; 
	  - Nếu delayTime càng lớn, chương trình càng bị chậm dần lại.
	  - delayTime sẽ lớn khi giá trị FRAME_PER_SECOND càng nhỏ (fps càng nhỏ => timePerFrame càng lớn => delayTime càng lớn). Vậy để game chạy nhanh 
	    thì tăng fps lên.
	  - Khi fps tăng tới tối đa (khi timePerFrame = realTime) thì chương trình sẽ đạt chất lượng tối đa.
	  - Muốn chất lượng hình ảnh càng tốt thì cần phải giảm realTime xuống thật nhỏ => Phụ thuộc vào cấu hình máy tính cao hay thấp.

Bài 9: Xử lý lớp đạn bắn:
	+ Xây dựng một lớp đạn bắn cho nhân vật, kế thừa từ lớp BaseObject.