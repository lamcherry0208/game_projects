﻿
#include "stdafx.h"
#include "BaseFunction.h"
#include "BaseObject.h"
#include "game_map.h"
#include "MainPlayer.h"
#include "ImpTimer.h"

BaseObject g_background;

bool InitData() // Khởi tạo các thông số cơ bản cho SDL.
{
	bool success = true;

	int ret = SDL_Init(SDL_INIT_VIDEO);
	if (ret < 0) // Hàm có lỗi => Init không thành công.
		return false;

	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"); // Thiết lập chất lượng.

	g_window = SDL_CreateWindow("Game Cpp SDL 2.0 - Lam Cherry", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
		                         SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if (g_window == NULL)
		success = false;
	else 
	{
		g_screen = SDL_CreateRenderer(g_window, -1, SDL_RENDERER_ACCELERATED);
		if (g_screen == NULL)
			success = false;
		else // Khởi tạo thành công màn hình game.
		{
			SDL_SetRenderDrawColor(g_screen, RENDER_DRAW_COLOR, RENDER_DRAW_COLOR, RENDER_DRAW_COLOR, RENDER_DRAW_COLOR); // Khởi tạo màu sắc cho màn hình.
			int imgFlags = IMG_INIT_PNG;
			if (!(IMG_Init(imgFlags) && imgFlags))
				success = false;
		}
	}

	return success;
}

bool LoadBackGround() //Tải ảnh background lên.
{
	bool ret = g_background.LoadImage("Img//background.png", g_screen); // Load image từ địa chỉ và đưa vào biến g_screen.

	if (ret == false)
		return false;

	return true;
}

void Close()
{
	g_background.Free();

	SDL_DestroyRenderer(g_screen);
	g_screen = NULL;

	SDL_DestroyWindow(g_window);
	g_window = NULL;

	IMG_Quit();
	SDL_Quit();
}

int main(int argc, char* argv[])
{
    ImpTimer fpsTimer;


	if (InitData() == false)
		return -1;

	if (LoadBackGround() == false)
		return -1;
	
	GameMap game_map_;
	game_map_.LoadMap("map/map01.dat");
	game_map_.loadTiles(g_screen);

    MainPlayer player;
    player.LoadImage("img//player_right.png", g_screen);
    player.setClip();

	bool is_quit = false;
	while (!is_quit) // Tạo vòng while để hiển thị background cho tới khi bấm thoát chương trình thì màn hình mới tắt. Vòng while để xử lý với mỗi lần thao tác.
	{
        fpsTimer.start();

		while (SDL_PollEvent(&g_event) != 0)
		{
			if (g_event.type == SDL_QUIT)
				is_quit = true;

            player.handleInputAction(g_event, g_screen);
		}
		// Trước khi hiển thị hình ảnh thì set lại màu cho màn hình và xóa màn hình cũ.
		SDL_SetRenderDrawColor(g_screen, RENDER_DRAW_COLOR, RENDER_DRAW_COLOR, RENDER_DRAW_COLOR, RENDER_DRAW_COLOR); 
		SDL_RenderClear(g_screen);

		g_background.Render(g_screen, NULL);
        Map mapData = game_map_.getMap();
        
        player.handleBullet(g_screen);
        player.setMapXY(mapData.startX, mapData.startY); // Bắt đầu tiến hành di chuyển map từ điểm bắt đầu của map ban đầu (0, 0).
        player.doPlayer(mapData);
        player.show(g_screen); // Hiển thị nhân vật ra màn hình.

        game_map_.setMap(mapData);
        game_map_.drawMap(g_screen);

		SDL_RenderPresent(g_screen);

        int realImpTime = fpsTimer.getTick(); // Thời gian thực sự đã trôi qua khi hiển thị 1 frame.
        int timePerFrame = 1000 / FRAME_PER_SECOND; // Mỗi frame sẽ chạy trong 1000 / 25 miliseconds.
        if (realImpTime < timePerFrame) // Nếu thời gian thực sự của 1 frame > thời gian quy định, ta cần tạo độ delay để 1 khung hình phải hiển thị đủ thời gian.
        {
            int delayTime = timePerFrame - realImpTime;
            if (delayTime >= 0)
                SDL_Delay(delayTime);
        }
	}

	Close();

	return 0;
}