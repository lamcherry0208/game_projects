﻿/*
  Class Object đại diện cho các đối tượng hình ảnh sẽ hiển thị trên màn hình game. Các đối tượng cụ thể trong game sẽ kế thừa từ class này.
  Trong class BaseObject, chúng ta định nghĩa cụ thể các hàm của lớp BaseObject đã khởi tạo sẵn trong file Header.
*/
#include "stdafx.h"
#include "BaseObject.h"

BaseObject::BaseObject()
{
	p_object_ = NULL;
	rect_.x = 0;
	rect_.y = 0;
	rect_.w = 0;
	rect_.h = 0;
}

BaseObject::~BaseObject() // Khi chương trình kết thúc thì hủy đối tượng đi.
{
	Free();
}

bool BaseObject::loadImage(std::string path, SDL_Renderer* screen) 
{

    Free();
	SDL_Texture* newTexture = NULL;

	SDL_Surface* loadSurface = IMG_Load(path.c_str()); // Đọc tấm ảnh từ đường dẫn có sẵn, đưa vào biến loadSurface.
	if (loadSurface != NULL) // Nếu load ảnh thành công
	{
		SDL_SetColorKey(loadSurface, SDL_TRUE, SDL_MapRGB(loadSurface -> format, COLOR_KEY_R, COLOR_KEY_G, COLOR_KEY_B)); 
		/* 
		  Kĩ thuật colorkey trong đồ họa: Khiến cho màu của background trùng màu với ảnh nhân vật => Nhân vật di chuyển như thật.
		  Đặt màu của background tại vị trí của nhân vật giống như màu của background nhân vật.
		*/
		newTexture = SDL_CreateTextureFromSurface(screen, loadSurface); // Đưa ảnh đã load lên vào screen.
		if (newTexture != NULL) // Load ảnh thành công thì phải gán các thông số của ảnh đã load lên vào đối tượng rect_
		{
			rect_.w = loadSurface -> w;
			rect_.h = loadSurface -> h;
		}

		SDL_FreeSurface(loadSurface); // Đã load xong hình ảnh, giải phóng biến loadSurface. 
	}

	p_object_ = newTexture;

	return p_object_ != NULL;
}

void BaseObject::Render(SDL_Renderer *des, const SDL_Rect *clip) // Đẩy hình ảnh (p_object_) lên vị trí des trên màn hình.
{
	SDL_Rect renderquad = {rect_.x, rect_.y, rect_.w, rect_.h}; // Vị trí và kích thước của tấm ảnh.

	SDL_RenderCopy(des, p_object_, clip, &renderquad);
}

void BaseObject::Free()
{
	if (p_object_ != NULL)
	{
		SDL_DestroyTexture(p_object_);
		p_object_ = NULL;
		rect_.w = rect_.h = 0;
	}
}