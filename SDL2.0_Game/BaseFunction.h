﻿/*
  File header khai báo trước các định danh, thư viện, các tên hàm, các tên biến cần sử dụng. Sau đó ta sẽ viết chi tiết code của chúng trong file cpp tương ứng.
  File này chứa tất cả các thông số cơ bản của project.
*/
#ifndef BASE_FUNCTION_H_
#define BASE_FUNCTION_H_

#include <windows.h>
#include <string>
#include <vector>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>

static SDL_Window *g_window = NULL; // Biến cửa sổ game.
static SDL_Renderer *g_screen = NULL; // Biến màn hình game.
static SDL_Event g_event; // Biến bắt sự kiện (các thao tác từ chuột và bàn phím).

// Screen
const int SCREEN_WIDTH = 1100; // Chiều dài màn hình.
const int SCREEN_HEIGHT = 640; // Chiều rộng màn hình.
const int SCREEN_BPP = 32; // Pixels.
const int FRAME_PER_SECOND = 25; // fps.

// Màu sắc trên màn hình
const int COLOR_KEY_R = 167;
const int COLOR_KEY_G = 175;
const int COLOR_KEY_B = 180;
const int RENDER_DRAW_COLOR = 0Xff;

// Game_Map
#define TILE_SIZE 64 // Kích thước một ô vuông trên bản đồ.
#define MAX_MAP_X 400 // Số lượng hoành độ bản đồ.
#define MAX_MAP_Y 10 // Số lượng tung độ bản đồ.

// Game Money
#define STATE_MONEY 4

typedef struct Map
{
	int startX, startY; // Vị trí của tile (chỉ số của ĐIỂM giao giữa các tile)
	int maxX, maxY; // Tọa độ ô thứ bao nhiêu trên bản đồ, chính bằng [start / TILE_SIZE] 
	int tile[MAX_MAP_Y][MAX_MAP_X]; // Lưu trạng thái của các ô trên bản đồ.
	char* file_name; // Con trỏ lưu địa chỉ file text chứa dữ liệu bản đồ.
};

// Nhân vật game
#define frameAmount 8

typedef struct Moving // Cấu trúc lưu việc di chuyển của nhân vật xem đang di chuyển kiểu gì.
{
    int left, right, up, down;
    int jump;
};

#endif

