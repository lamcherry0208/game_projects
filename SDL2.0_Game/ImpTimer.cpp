﻿
#include "stdafx.h"
#include "ImpTimer.h"
#include "BaseFunction.h"

ImpTimer :: ImpTimer()
{
    startTick = 0;
    pausedTick = 0;
    is_Paused = false;
    is_Started = false;
}

ImpTimer :: ~ImpTimer()
{

}

void ImpTimer :: start()
{
    is_Started = true;
    is_Paused = false;
    startTick = SDL_GetTicks(); // Lấy số lượng miliseconds kể từ thời điểm thư viện SDL được khởi tạo.
}

void ImpTimer :: stop()
{
    is_Paused = is_Started = true;
}

void ImpTimer :: paused()
{
    if (is_Started == true && is_Paused == false)
    {
        is_Paused = true;
        pausedTick = SDL_GetTicks() - startTick; // Khi paused lại thì thời điểm paused bằng thời gian tính tới hiện tại - thời gian lúc khởi tạo thư viện SDL.
    }
}

void ImpTimer :: unPaused()
{
    if (is_Paused == true)
    {
        is_Paused = false;
        startTick = SDL_GetTicks() - pausedTick;
        pausedTick = 0;
    }
}

int ImpTimer :: getTick() // Lấy thời điểm hiện tại trong game.
{
    if (is_Started == true)
    {
        if (is_Paused == true)
            return pausedTick;
        else 
            return SDL_GetTicks() - startTick;
    }

    return 0;
}

bool ImpTimer :: isStarted()
{
    return is_Started;
}

bool ImpTimer :: isPaused()
{
    return is_Started;
}