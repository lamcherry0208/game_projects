﻿
#ifndef BULLET_OBJECT_H_
#define BULLET_OBJECT_H_

#include "BaseObject.h"
#include "BaseFunction.h"

class BulletObject : public BaseObject
{
    public:
        BulletObject();

        ~BulletObject();

        enum BulletDir
        {
            DIR_RIGHT = 20,
            DIR_LEFT = 21,
            DIR_UP = 22,
            DIR_UP_LEFT = 23,
            DIR_UP_RIGHT = 24,
            DIR_DOWN_LEFT = 25,
            DIR_DOWN_RIGHT = 26,
        };

        enum BulletType
        {
            BASE_BULLET = 50,
            LASER_BULLET = 51,
        };

        void setX_val(const int& xVal) { x_val = xVal; } // Getter.
        
        void setY_val(const int& yVal) { y_val = yVal; } // Setter.

        int getX_val() const { return x_val; }

        int getY_val() const { return y_val; }

        void setIsMoved(const bool& is_moved) { isMoved = is_moved; }

        bool getIsMoved() const { return isMoved; }

        void handleMove(const int& xBorder, const int& yBorder); // Hàm ghi nhận viên đạn có còn ở trong giới hạn bản đồ hay không.

        void setBulletDir(const unsigned int& bulletDir) { bulletDirect = bulletDir; }

        unsigned int getBulletDir() const { return bulletDirect; }

        void setBulletType(const unsigned int& type) { bulletType = type; }

        unsigned int getBulletType() const { return bulletType; }

        void loadBulletIMG(SDL_Renderer* des);

        void loadBulletDirectIMG(SDL_Renderer* des); // Hàm load ảnh nhân vật đưa súng theo các hướng phụ thuộc vào hướng chuột.

    private:
        int x_val, y_val; // Lưu lượng giá trị thay đổi vị trí của viên đạn khi di chuyển trên bản đồ.
        bool isMoved; // Viên đạn đang ở trong màn hình thì sẽ được đánh dấu là đang di chuyển, ngược lại thì không di chuyển và lợi dụng việc này để xóa nó đi.
        unsigned int bulletDirect; // Hướng viên đạn đang sang trái hay sang phải.
        unsigned int bulletType; // Loại đạn hiển thị (sau này phát triển tính năng ăn các loại đạn khác nhau).
};

#endif