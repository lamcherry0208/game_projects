﻿
#include "stdafx.h"
#include "MainPlayer.h"

MainPlayer :: MainPlayer()
{
    frameIndex = 0;
    x_pos = y_pos = 0;
    x_val = y_val = 0;
    frameWidth = frameHeight = 0;
    movingStatus = WALK_NONE;
    moveType.left = moveType.right = moveType.up = moveType.down = moveType.jump = 0;
    onGround = false;
    mapX = mapY = 0;
    comeBackTime = 0;
}

MainPlayer :: ~MainPlayer()
{

}

bool MainPlayer :: loadImage(std::string path, SDL_Renderer* screen)
{
    bool ret = BaseObject::loadImage(path, screen); // Tái sử dụng hàm LoadImage từ lớp cha, sau đó bổ sung một vài thông số riêng.

    if (ret == true) // Nếu tải ảnh thành công, ta lấy kích thước thực sự của nhân vật = 1 frame (tổng kích thước ảnh chia cho số frame)
    {
        frameWidth = rect_.w / frameAmount;
        frameHeight = rect_.h;
    }

    return ret;
}

void MainPlayer :: setClip()
{
    if (frameWidth > 0 && frameHeight > 0)
    {
        // Coi vị trí của 1 frame là tọa độ đỉnh trái trên của frame, cạnh trên của frame là trục Ox.
        //frameClip[0].x = frameClip[0].y = 0;
        //frameClip[0].w = frameWidth;
        //frameClip[0].h = frameHeight;

        for (int i = 0; i < 8; ++i)
        {
            frameClip[i].x = i * frameWidth; // Vị trí bắt đầu trên trục hoành của frame thứ i bằng i * độ rộng 1 frame
            frameClip[i].y = 0;
            frameClip[i].w = frameWidth;
            frameClip[i].h = frameHeight;
        }
    }
}

void MainPlayer :: updatePlayerIMG(SDL_Renderer* des)
{
    if (onGround == true) // Nếu nhân vật đang ở trên mặt đất thì load ảnh di chuyển mặt đất, ngược lại load ảnh không trung.
    {
        if (movingStatus == WALK_LEFT) // Load ảnh tương ứng với việc nhân vật sang phải hay sang trái.
        {
            loadImage("img//player_left.png", des);
        }
        else 
        {
            loadImage("img//player_right.png", des);
        }
    }
    else // neu onGround = false, nghia la dang o tren ko trung
    {
        if (movingStatus == WALK_LEFT) // neu dang huong sang trai, thi load anh jump trai
        {
            loadImage("img//jump_left.png", des);
        }
        else 
        {
            loadImage("img//jump_right.png", des);
        }
    }
}

void MainPlayer :: show(SDL_Renderer* des)
{
    updatePlayerIMG(des); // Cập nhật ảnh load lên cho nhân vật.

    if (moveType.left == 1 || moveType.right == 1) // Nếu liên tục ấn nút di chuyển sang cùng phía thì frame nhân vật sẽ thay đổi.
        ++frameIndex;
    else 
        frameIndex = 0;

    if (frameIndex > 7) // Nếu load hết dải frame thì lại quay về load từ frame ảnh đầu tiên.
    {
        frameIndex = 0;
    }

    if (comeBackTime == 0) // Khi nhân vật không bị rơi, đang ở trong bản đồ thì mới tiến hành load hình ảnh nhân vật, đỡ tốn thời gian.
    {
        rect_.x = x_pos - mapX; // Khi chạy nhân vật, màn hình bị cuốn theo nên khung hình phải trừ đi phần đã bị cuốn trôi ra phía trước. 
        rect_.y = y_pos - mapY;

        SDL_Rect* currentClip = &frameClip[frameIndex]; // Hình ảnh hiện tại chính bằng frame thứ index.

        SDL_Rect renderQuad = {rect_.x, rect_.y, frameWidth, frameHeight}; // Kích thước và vị trí chính xác của frame hiện tại.

        SDL_RenderCopy(des, p_object_, currentClip, &renderQuad); // Render phần currentClip của p_object (cả dải frame) vào des, theo kích thước hình chữ nhật renderQuad.
    }
    
}

void MainPlayer :: handleInputAction(SDL_Event events, SDL_Renderer* screen)
{
    if (events.type == SDL_KEYDOWN) // Khi bắt được sự kiện nhấn phím xuống.
    {
        switch (events.key.keysym.sym)
        {
            case SDLK_RIGHT:
                {
                 movingStatus = WALK_RIGHT;
                 moveType.right = 1;
                 moveType.left = 0;

                }
                break;
            case SDLK_LEFT:
                {
                 movingStatus = WALK_LEFT;
                 moveType.left = 1;
                 moveType.right = 0;
                }
                break;

            case SDLK_UP:
                {
                  moveType.jump = 1;
                }
                break;

            default:
                break;
        }
    }
    else if (events.type == SDL_KEYUP) // Khi bắt sự kiện nhả phím ra.
    {
        switch (events.key.keysym.sym)
        {
            case SDLK_RIGHT:
                 moveType.right = 0;
                break;

            case SDLK_LEFT:
                 moveType.left = 0;
                break;

            case SDLK_UP:
                 moveType.jump = 0;
                break;
        }
    }
    
    if (events.type == SDL_MOUSEBUTTONDOWN) // Bắt sự kiện nhả đạn khi bấm chuột trái xuống.
    {
        if (events.button.button == SDL_BUTTON_LEFT)
        {
            if (events.button.state == SDL_PRESSED)
            {
                // Tạo ra viên đạn mới để bắn ra.
                BulletObject* p_bullet = new BulletObject();
                p_bullet -> setBulletType(BulletObject :: BASE_BULLET);
                p_bullet -> loadBulletIMG(screen);

                if (movingStatus == WALK_LEFT)
                {
                    p_bullet -> setBulletDir(BulletObject::DIR_LEFT); // Đặt hướng bắn của viên đạn.
                    p_bullet -> SetRect(this -> rect_.x, rect_.y + frameHeight * 0.25); // Tạo vị trí cho viên đạn bắt đầu nhả ra cho gần với súng.
                }
                else
                {
                    p_bullet -> setBulletDir(BulletObject::DIR_RIGHT);
                    p_bullet -> SetRect(this -> rect_.x + frameWidth - 20, rect_.y + frameHeight * 0.25); // Tạo vị trí cho viên đạn bắt đầu nhả ra cho gần với súng.
                }

                p_bullet -> setX_val(20); // Set độ dài bay ngang của đạn.
                p_bullet -> setY_val(20); // Set độ dài bay dọc của đạn.
                p_bullet -> setIsMoved(true);  // Đặt trạng thái di chuyển của viên đạn là true, tức đang hiển thị trên màn hình.

                p_bullet_list.push_back(p_bullet); // Nạp viên đạn vào danh sách bắn.
            }
        }
    }
}

void MainPlayer :: handleBullet(SDL_Renderer* des) 
{
    for (int i = 0; i < p_bullet_list.size(); ++i)
    {
        BulletObject* p_bullet = p_bullet_list.at(i);
        if (p_bullet != NULL)
        {
            if (p_bullet -> getIsMoved() == true) // Khi viên đạn đang hiển thị trên màn hình.
            {
                p_bullet -> handleMove(SCREEN_WIDTH, SCREEN_HEIGHT); // Khi viên đạn đang bắn ra thì cứ liên tục cộng thêm lượng di chuyển tới khi hết màn hình.
                p_bullet -> Render(des);
            }
            else // Nếu viên đạn đã ra khỏi màn hình thì xóa nó đi.
            {
                p_bullet_list.erase(p_bullet_list.begin() + i);

                if (p_bullet != NULL)
                {
                    delete p_bullet;
                    p_bullet = NULL;
                }
            }
        }
    }
}

void MainPlayer :: increaseMoney()
{
    ++moneyReceived;
}

void MainPlayer :: doPlayer(Map& mapData)
{
    if (comeBackTime == 0) // Khi comeBackTime = 0 thì nhân vật đang ở trong bản đồ (còn sống) => xử lý kiểm tra di chuyển.
    {
        x_val = 0;
        y_val += 0.8;

        if (y_val >= MAX_FALL_SPEED)
            y_val = MAX_FALL_SPEED;

        if (moveType.left == 1) // Di chuyển sang trái thì hoành độ giảm đi đúng bằng player_speed.
        {
            x_val -= PLAYER_SPEED;
        }
        else if (moveType.right == 1) // Ngược lại di chuyển sang phải.
        {
            x_val += PLAYER_SPEED;
        }

        if (moveType.jump == 1) // Nếu phát hiện nhấn nhảy lên trên.
        {
            if (onGround == true)  // Chỉ khi nào nhân vật đang ở trên mặt đất thì mới được phép nhảy lên.
            {
                y_val = -PLAYER_JUMP_VAL; // Set lượng nhảy lên phía trên theo chiều dọc.
                onGround = false;
            }

            moveType.jump = 0; // Đã nhảy xong.
        }

        // Khi nhân vật di chuyển thì vừa phải xử lý va chạm với bản đồ, vừa phải cuốn bản đồ theo bước di chuyển của nhân vật.
        checkToMap(mapData); 
        centerEntityOnMap(mapData);
    }
    
    if (comeBackTime > 0)
    {
        --comeBackTime;
        if (comeBackTime == 0) // Khi nhân vật đã quay trở lại màn hình thì coi như bắt đầu lại từ vị trí gần mặt đất để có thể hồi sinh lại.
        {
            if (x_pos > 256) // Nếu vị trí nhân vật lúc rơi ở khoảng giữa bản đồ thì lùi lại chút để dễ điều khiển nv vào mặt đất hơn.
            {
                x_pos -= 256; // Trả nhân vật về vị trí trước đó khoảng 4 tile map.
            }
            else // Nếu ở gần mép phải bản đồ thì lùi lại 1 ô tile thôi.
            {
                x_pos -= TILE_SIZE;
            }
            y_pos = 0;
            x_val = y_val = 0; // Coi như bắt đầu lại từ đầu
        }
    }
}

void MainPlayer :: centerEntityOnMap(Map& mapData)
{
    mapData.startX = x_pos - (SCREEN_WIDTH / 2); // Khi nhân vật bắt đầu di chuyển tới giữa bản đồ thì sẽ bắt đầu cuốn bản đồ theo.
    if (mapData.startX < 0) // Nếu như bản đồ bị lùi thì sẽ không lùi quá nửa bản đồ.
        mapData.startX = 0;
    else if (mapData.startX + SCREEN_WIDTH >= mapData.maxX) // Nếu bản đồ bị đi quá giới hạn tối đa của nó thì gán lại vị trí bằng vị trí thực tế của bản đồ.
        mapData.startX = mapData.maxX - SCREEN_WIDTH;

    mapData.startY = y_pos - (SCREEN_HEIGHT / 2); // Xử lý tương tự khi bản đồ di chuyển dọc.
    if (mapData.startY < 0)
        mapData.startY = 0;
    else if (mapData.startY + SCREEN_HEIGHT > mapData.maxY)
        mapData.startY = mapData.maxY - SCREEN_HEIGHT;
}

void MainPlayer :: checkToMap(Map& mapData)
{
    int x1 = 0, x2 = 0; // Giới hạn 1 đoạn kiểm tra trên trục hoành.
    int y1 = 0, y2 = 0; // Giới hạn 1 đoạn kiểm tra trên trục tung.

    // Check chiều ngang trước
    int heightMin = frameHeight < TILE_SIZE ? frameHeight : TILE_SIZE;
    // Xác định xem nhân vật đang nằm từ ô thứ bao nhiêu tới ô thứ bao nhiêu trên bản đồ.
    x1 = (x_pos + x_val) / TILE_SIZE; // xác định nhân vật đang đứng ở ô thứ bao nhiêu, lấy vị trí hoành độ chia cho kích thước tile.
    x2 = (x_pos + x_val + frameWidth - 1) / TILE_SIZE; // Điểm kết thúc của nhân vật, trừ 1 để đường biên của nhân vật k bị trùng đường biên của tile.
    y1 = (y_pos + y_val) / TILE_SIZE;
    y2 = (y_pos + heightMin - 1) / TILE_SIZE;

    if (x1 >= 0 && x2 < MAX_MAP_X && y1 >= 0 && y2 < MAX_MAP_Y) // Nếu nhân vật đang nằm trong bản đồ.
    {
        if (x_val > 0) // Nhân vật đang di chuyển sang phải.
        {
            int val1 = mapData.tile[y1][x2];
            int val2 = mapData.tile[y2][x2];

            if (val1 == STATE_MONEY || val2 == STATE_MONEY) // Xử lý va chạm với đồng tiền trên bản đồ (ăn được tiền).
            {
                mapData.tile[y1][x2] = mapData.tile[y2][x2] = 0; // Xóa đồng tiền đi vì đã ăn được rồi.
                increaseMoney();
            }
            else 
            {
                if (val1 != 0 || val2 != 0) // Nhân vật đang chạm vào 1 ô có chướng ngại vật.
                {
                    x_pos = x2 * TILE_SIZE; 
                    x_pos -= frameWidth + 1; // Trả  vị trí của nhân vật về vị trí ngay trước vị trí bị va chạm.
                    x_val = 0; // Gán lượng di chuyển về 0, kể cả có ấn di chuyển tiếp cũng vẫn sẽ bị va chạm.

                    onGround = true;

                    if (movingStatus == WALK_NONE) // Nếu nhân vật mới hiện lên, chưa có trạng thái di chuyển thì coi như bắt đầu đi sang phải.
                    {
                        movingStatus = WALK_RIGHT; 
                    }
                }
            }
        }
        else if (x_val < 0) // Đang di chuyển sang trái.
        {
            int val1 = mapData.tile[y1][x1];
            int val2 = mapData.tile[y2][x1];

            if (val1 == STATE_MONEY || val2 == STATE_MONEY) // Xử lý va chạm với đồng tiền trên bản đồ (ăn được tiền).
            {
                mapData.tile[y1][x1] = mapData.tile[y2][x1] = 0; // Xóa đồng tiền đi vì đã ăn được rồi.
                increaseMoney();
            }
            else if (val1 != 0 || val2 != 0) // Vẫn là ô vật cản nhưng không phải tiền.
            {
                x_pos = (x1 + 1) * TILE_SIZE;
                x_val = 0;
            }
        }
    }

    // Check chiều dọc xem có va chạm không.
    int widthMin = frameWidth < TILE_SIZE ? frameWidth : TILE_SIZE;
    x1 = x_pos / TILE_SIZE;
    x2 = (x_pos + widthMin) / TILE_SIZE;
    y1 = (y_pos + y_val) / TILE_SIZE;
    y2 = (y_pos + y_val + frameHeight - 1) / TILE_SIZE;

    if (x1 >= 0 && x2 < MAX_MAP_X && y1 >= 0 && y2 < MAX_MAP_Y)
    {
        if (y_val > 0)
        {
            int val1 = mapData.tile[y2][x1];
            int val2 = mapData.tile[y2][x2];

            if (val1 == STATE_MONEY || val2 == STATE_MONEY)
            {
                mapData.tile[y2][x1] = mapData.tile[y2][x2] = 0;
                increaseMoney();
            }
            else if (val1 != 0 || val2 != 0)
            {
                y_pos = y2 * TILE_SIZE;
                y_pos -= (frameHeight + 1);
                y_val = 0;
                onGround = true;
                moveType.jump = 0;
            }
        }
        else if (y_val < 0)
        {
            int val1 = mapData.tile[y1][x1];
            int val2 = mapData.tile[y1][x2];

            if (val1 == STATE_MONEY || val2 == STATE_MONEY)
            {
                mapData.tile[y1][x1] = mapData.tile[y1][x2] = 0;
                increaseMoney();
            }
            else if (val1 != 0 || val2 != 0)
            {
                y_pos = (y1 + 1) * TILE_SIZE;
                y_val = 0;
            }
        }
    }

    x_pos += x_val;
    y_pos += y_val;

    if (x_pos < 0)
    {
        x_pos = 0;
    }
    else if (x_pos + frameWidth > mapData.maxX)
    {
        x_pos = mapData.maxX - frameWidth - 1;
    }

    if (y_pos > mapData.maxY) // Khi nhân vật rơi xuống => vị trí chiều dọc của nhân vật bị vượt quá số ô chiều dọc của bản đồ.
        comeBackTime = 30; // Set thời gian cho nhân vật quay trở lại bản đồ delay đi một chút = 30 milisec
}